package cz.com.example.instazoo.domain.util;

/**
 * @author : vanya.melnykovych
 * @since : 03.05.2021
 */
public enum Role {
    ROLE_USER,
    ROLE_ADMIN
}
