package cz.com.example.instazoo.domain;

import cz.com.example.instazoo.domain.util.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

/**
 * @author : vanya.melnykovych
 * @since : 03.05.2021
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Post extends BaseEntity {

    private String title;

    private String caption;

    private String location;

    private Integer likes;

    @ManyToOne(fetch = FetchType.LAZY)
    private User creator;

    @Column
    @ElementCollection(targetClass = String.class)
    private Set<String> likedUsers;

    @OneToMany(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER,
    mappedBy = "post", orphanRemoval = true)
    private List<Comment> comments;
}
