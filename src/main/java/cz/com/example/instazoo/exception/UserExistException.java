package cz.com.example.instazoo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author : vanya.melnykovych
 * @since : 03.05.2021
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class UserExistException extends RuntimeException {

    public UserExistException(String message) {
        super(message);
    }

}
