package cz.com.example.instazoo.payload.request;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * @author : vanya.melnykovych
 * @since : 03.05.2021
 */
@Data
public class LoginRequest {

    @NotEmpty(message = "Username cannot be empty")
    private String username;

    @NotEmpty(message = "Password cannot be empty")
    private String password;
}
