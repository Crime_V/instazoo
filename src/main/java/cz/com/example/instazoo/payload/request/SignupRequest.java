package cz.com.example.instazoo.payload.request;

import cz.com.example.instazoo.annotation.PasswordMatches;
import cz.com.example.instazoo.annotation.ValidEmail;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

/**
 * @author : vanya.melnykovych
 * @since : 03.05.2021
 */
@Data
@PasswordMatches
public class SignupRequest {

    @ValidEmail
    @NotBlank(message = "User email is required")
    @Email(message = "It should have email format")
    private String email;

    @NotEmpty(message = "Please enter your first name")
    private String firstname;

    @NotEmpty(message = "Please enter your last name")
    private String lastname;

    @NotEmpty(message = "Please enter your username")
    private String username;

    @Size(min = 6)
    @NotEmpty(message = "Password is required")
    private String password;

    private String confirmPassword;
}
