package cz.com.example.instazoo.payload.response;

import lombok.Getter;

/**
 * @author : vanya.melnykovych
 * @since : 03.05.2021
 */
@Getter
public class InvalidLoginResponse {

    private String username;

    private String password;

    public InvalidLoginResponse() {
        this.username = "Invalid username";
        this.password = "Invalid password";
    }
}
