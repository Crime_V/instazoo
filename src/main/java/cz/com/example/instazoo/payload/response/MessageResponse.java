package cz.com.example.instazoo.payload.response;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author : vanya.melnykovych
 * @since : 03.05.2021
 */
@Data
@AllArgsConstructor
public class MessageResponse {

    private String message;
}
