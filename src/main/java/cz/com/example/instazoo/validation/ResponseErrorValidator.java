package cz.com.example.instazoo.validation;

import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * @author : vanya.melnykovych
 * @since : 03.05.2021
 */
@Service
public class ResponseErrorValidator {

    public ResponseEntity<Object> getErrorResponse(BindingResult bindingResult) {
        Map<String, String> errorMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(bindingResult.getAllErrors())) {
            errorMap.putAll(getObjectErrors(bindingResult));
        }
        errorMap.putAll(getFieldErrors(bindingResult));
        return new ResponseEntity<>(errorMap, HttpStatus.BAD_REQUEST);
    }

    private Map<String, String> getObjectErrors(BindingResult bindingResult) {
        Collector<ObjectError, ?, Map<String, String>> collector = Collectors.toMap(
                DefaultMessageSourceResolvable::getCode,
                ObjectError::getDefaultMessage);
        return bindingResult.getAllErrors().stream().collect(collector);
    }

    private Map<String, String> getFieldErrors(BindingResult bindingResult) {
        Collector<FieldError, ?, Map<String, String>> collector = Collectors.toMap(
                FieldError::getField,
                FieldError::getDefaultMessage);
        return bindingResult.getFieldErrors().stream().collect(collector);
    }
}
