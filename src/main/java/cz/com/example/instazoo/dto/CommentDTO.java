package cz.com.example.instazoo.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * @author : vanya.melnykovych
 * @since : 04.05.2021
 */
@Data
public class CommentDTO {

    private Long id;

    @NotEmpty
    private String message;

    private String username;
}
