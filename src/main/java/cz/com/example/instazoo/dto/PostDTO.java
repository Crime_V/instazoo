package cz.com.example.instazoo.dto;

import lombok.Data;

import java.util.Set;

/**
 * @author : vanya.melnykovych
 * @since : 04.05.2021
 */
@Data
public class PostDTO {

    private Long id;

    private String title;

    private String caption;

    private String location;
    
    private String username;

    private Integer likes;

    private Set<String> usersLiked;
}
