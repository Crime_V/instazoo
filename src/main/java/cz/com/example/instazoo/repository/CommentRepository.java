package cz.com.example.instazoo.repository;

import cz.com.example.instazoo.domain.Comment;
import cz.com.example.instazoo.domain.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author : vanya.melnykovych
 * @since : 03.05.2021
 */
@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {

    List<Comment> findAllByPost(Post post);

    Comment findByIdAndUserId(Long commentId, Long userId);
}
