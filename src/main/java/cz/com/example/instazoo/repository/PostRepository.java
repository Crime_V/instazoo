package cz.com.example.instazoo.repository;

import cz.com.example.instazoo.domain.Post;
import cz.com.example.instazoo.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author : vanya.melnykovych
 * @since : 03.05.2021
 */
@Repository
public interface PostRepository extends JpaRepository<Post, Long> {

    List<Post> findAllByCreatorOrderByCreatedDesc(User creator);

    List<Post> findAllByOrderByCreatedDesc();
    
    Optional<Post> findPostByIdAndCreator(Long id, User creator);
}
