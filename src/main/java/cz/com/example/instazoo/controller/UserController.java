package cz.com.example.instazoo.controller;

import cz.com.example.instazoo.domain.User;
import cz.com.example.instazoo.dto.UserDTO;
import cz.com.example.instazoo.facade.UserFacade;
import cz.com.example.instazoo.service.UserService;
import cz.com.example.instazoo.validation.ResponseErrorValidator;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;

/**
 * @author : vanya.melnykovych
 * @since : 04.05.2021
 */
@RestController
@RequestMapping("api/user")
public class UserController {

    private final UserFacade userFacade;

    private final UserService userService;

    private final ResponseErrorValidator responseErrorValidator;

    public UserController(UserFacade userFacade, UserService userService, ResponseErrorValidator responseErrorValidator) {
        this.userFacade = userFacade;
        this.userService = userService;
        this.responseErrorValidator = responseErrorValidator;
    }

    @GetMapping("/current")
    public ResponseEntity<UserDTO> getCurrentUser(Principal principal) {
        User user =  userService.getCurrentUser(principal);
        UserDTO userDTO = userFacade.userToUserDTO(user);
        return new ResponseEntity<>(userDTO, HttpStatus.OK);
    }

    @GetMapping("/{userId}")
    public ResponseEntity<UserDTO> getUserProfile(@PathVariable Long userId) {
        User user = userService.getUserById(userId);
        UserDTO userDTO = userFacade.userToUserDTO(user);
        return new ResponseEntity<>(userDTO, HttpStatus.OK);
    }

    @PostMapping("/update")
    public ResponseEntity<Object> updateUser(@RequestBody @Valid UserDTO userDTO,
                                             BindingResult bindingResult,
                                             Principal principal) {
        if (bindingResult.hasErrors()) {
            return responseErrorValidator.getErrorResponse(bindingResult);
        }
        User user = userService.updateUser(userDTO, principal);
        UserDTO userUpdated = userFacade.userToUserDTO(user);
        return new ResponseEntity<>(userUpdated, HttpStatus.OK);
    }
}
