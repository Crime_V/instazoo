package cz.com.example.instazoo.controller;

import cz.com.example.instazoo.domain.Comment;
import cz.com.example.instazoo.dto.CommentDTO;
import cz.com.example.instazoo.facade.CommentFacade;
import cz.com.example.instazoo.payload.response.MessageResponse;
import cz.com.example.instazoo.service.CommentService;
import cz.com.example.instazoo.validation.ResponseErrorValidator;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author : vanya.melnykovych
 * @since : 05.05.2021
 */
@RestController
@RequestMapping("api/comment")
public class CommentController {

    private final CommentFacade commentFacade;

    private final CommentService commentService;

    private final ResponseErrorValidator responseErrorValidator;

    public CommentController(CommentFacade commentFacade, CommentService commentService, ResponseErrorValidator responseErrorValidator) {
        this.commentFacade = commentFacade;
        this.commentService = commentService;
        this.responseErrorValidator = responseErrorValidator;
    }

    @PostMapping("/{postId}/create")
    public ResponseEntity<Object> createComment(@RequestBody @Valid CommentDTO commentDTO,
                                                BindingResult bindingResult,
                                                @PathVariable Long postId,
                                                Principal principal) {
        if (bindingResult.hasErrors()) {
            return responseErrorValidator.getErrorResponse(bindingResult);
        }
        Comment comment = commentService.saveComment(postId, commentDTO, principal);
        CommentDTO createdComment = commentFacade.commentToCommentDTO(comment);
        return new ResponseEntity<>(createdComment, HttpStatus.OK);
    }

    @GetMapping("/{postId}/all")
    public ResponseEntity<List<CommentDTO>> getAllPostComments(@PathVariable Long postId) {
        List<CommentDTO> postComments = commentService.getAllPostComments(postId).stream()
                .map(commentFacade::commentToCommentDTO)
                .collect(Collectors.toList());
        return new ResponseEntity<>(postComments, HttpStatus.OK);
    }

    @PostMapping("/{commentId}/delete")
    public ResponseEntity<MessageResponse> deleteComment(@PathVariable Long commentId) {
        commentService.deleteComment(commentId);
        return new ResponseEntity<>(new MessageResponse("Post was deleted"), HttpStatus.OK);
    }
}
