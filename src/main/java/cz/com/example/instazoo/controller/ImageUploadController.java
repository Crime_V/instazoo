package cz.com.example.instazoo.controller;

import cz.com.example.instazoo.domain.ImageModel;
import cz.com.example.instazoo.payload.response.MessageResponse;
import cz.com.example.instazoo.service.ImageUploadService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.Principal;

/**
 * @author : vanya.melnykovych
 * @since : 05.05.2021
 */
@RestController
@RequestMapping("api/image")
public class ImageUploadController {

    private final ImageUploadService imageUploadService;

    public ImageUploadController(ImageUploadService imageUploadService) {
        this.imageUploadService = imageUploadService;
    }

    @PostMapping("/upload")
    public ResponseEntity<MessageResponse> uploadImageToUser(@RequestParam MultipartFile file,
                                                             Principal principal) throws IOException {
        imageUploadService.uploadImageToUser(file, principal);
        return new ResponseEntity<>(new MessageResponse("Image to user was upload successfully"), HttpStatus.OK);
    }

    @PostMapping("/{postId}/upload")
    public ResponseEntity<MessageResponse> uploadImageToPost(@RequestParam MultipartFile file,
                                                             @PathVariable Long postId,
                                                             Principal principal) throws IOException {
        imageUploadService.uploadImageToPost(file, principal, postId);
        return new ResponseEntity<>(new MessageResponse("Image to post was upload successfully"), HttpStatus.OK);
    }

    @GetMapping("/profile")
    public ResponseEntity<ImageModel> getUserProfileImage(Principal principal) {
        ImageModel userImage = imageUploadService.getUserImage(principal);
        return new ResponseEntity<>(userImage, HttpStatus.OK);
    }

    @GetMapping("/{postId}/image")
    public ResponseEntity<ImageModel> getPostImage(@PathVariable Long postId) {
        ImageModel postImage = imageUploadService.getPostImage(postId);
        return new ResponseEntity<>(postImage, HttpStatus.OK);
    }
}
