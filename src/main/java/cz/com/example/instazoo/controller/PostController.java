package cz.com.example.instazoo.controller;

import cz.com.example.instazoo.domain.Post;
import cz.com.example.instazoo.dto.PostDTO;
import cz.com.example.instazoo.facade.PostFacade;
import cz.com.example.instazoo.payload.response.MessageResponse;
import cz.com.example.instazoo.service.PostService;
import cz.com.example.instazoo.validation.ResponseErrorValidator;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author : vanya.melnykovych
 * @since : 05.05.2021
 */
@RestController
@RequestMapping("api/post")
public class PostController {

    private final PostFacade postFacade;

    private final PostService postService;

    private final ResponseErrorValidator responseErrorValidator;

    public PostController(PostFacade postFacade, PostService postService, ResponseErrorValidator responseErrorValidator) {
        this.postFacade = postFacade;
        this.postService = postService;
        this.responseErrorValidator = responseErrorValidator;
    }

    @PostMapping("/create")
    public ResponseEntity<Object> createPost(@RequestBody @Valid PostDTO postDTO,
                                             BindingResult bindingResult,
                                             Principal principal) {
        if (bindingResult.hasErrors()) {
            return responseErrorValidator.getErrorResponse(bindingResult);
        }
        Post post = postService.createPost(postDTO, principal);
        PostDTO createdPost = postFacade.postToPostDTO(post);
        return new ResponseEntity<>(createdPost, HttpStatus.OK);
    }

    @GetMapping("/all")
    public ResponseEntity<List<PostDTO>> getAllPosts() {
        List<PostDTO> postDTOList = postService.getAllPosts().stream()
                .map(postFacade::postToPostDTO)
                .collect(Collectors.toList());
        return new ResponseEntity<>(postDTOList, HttpStatus.OK);
    }

    @GetMapping("/user/posts")
    public ResponseEntity<List<PostDTO>> getAllUserPosts(Principal principal) {
        List<PostDTO> userPosts = postService.getUserPosts(principal).stream()
                .map(postFacade::postToPostDTO)
                .collect(Collectors.toList());
        return new ResponseEntity<>(userPosts, HttpStatus.OK);
    }

    @PostMapping("/{postId}/{username}/like")
    public ResponseEntity<PostDTO> likePost(@PathVariable Long postId,
                                            @PathVariable String username) {
        Post post = postService.likePost(postId, username);
        PostDTO postDTO = postFacade.postToPostDTO(post);

        return new ResponseEntity<>(postDTO, HttpStatus.OK);
    }

    @PostMapping("/{postId}/delete")
    public ResponseEntity<MessageResponse> deletePost(@PathVariable Long postId, Principal principal) {
        postService.deletePost(postId, principal);
        return new ResponseEntity<>(new MessageResponse("Post was deleted"), HttpStatus.OK);
    }
}
