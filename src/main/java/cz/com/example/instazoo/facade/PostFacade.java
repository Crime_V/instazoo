package cz.com.example.instazoo.facade;

import cz.com.example.instazoo.domain.Post;
import cz.com.example.instazoo.dto.PostDTO;
import org.springframework.stereotype.Component;

/**
 * @author : vanya.melnykovych
 * @since : 04.05.2021
 */
@Component
public class PostFacade {

    public PostDTO postToPostDTO(Post post) {
        PostDTO postDTO = new PostDTO();
        postDTO.setId(post.getId());
        postDTO.setUsername(post.getCreator().getUsername());
        postDTO.setTitle(post.getTitle());
        postDTO.setCaption(post.getCaption());
        postDTO.setLikes(post.getLikes());
        postDTO.setUsersLiked(post.getLikedUsers());
        postDTO.setLocation(post.getLocation());
        return postDTO;
    }
}
