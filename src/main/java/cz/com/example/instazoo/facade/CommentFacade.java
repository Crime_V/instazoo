package cz.com.example.instazoo.facade;

import cz.com.example.instazoo.domain.Comment;
import cz.com.example.instazoo.dto.CommentDTO;
import org.springframework.stereotype.Component;

/**
 * @author : vanya.melnykovych
 * @since : 04.05.2021
 */
@Component
public class CommentFacade {

    public CommentDTO commentToCommentDTO(Comment comment) {
        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setId(comment.getId());
        commentDTO.setMessage(comment.getMessage());
        commentDTO.setUsername(comment.getUsername());
        return commentDTO;
    }
}
