package cz.com.example.instazoo.service;

import cz.com.example.instazoo.domain.ImageModel;
import cz.com.example.instazoo.domain.Post;
import cz.com.example.instazoo.domain.User;
import cz.com.example.instazoo.dto.PostDTO;
import cz.com.example.instazoo.exception.PostNotFoundException;
import cz.com.example.instazoo.repository.ImageRepository;
import cz.com.example.instazoo.repository.PostRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

/**
 * @author : vanya.melnykovych
 * @since : 04.05.2021
 */
@Service
public class PostService {

    public static final Logger LOG = LoggerFactory.getLogger(PostService.class);

    private final PostRepository postRepository;

    private final UserService userService;

    private final ImageRepository imageRepository;

    public PostService(PostRepository postRepository, UserService userService, ImageRepository imageRepository) {
        this.postRepository = postRepository;
        this.userService = userService;
        this.imageRepository = imageRepository;
    }

    public Post createPost(PostDTO postDTO, Principal principal) {
        User user = userService.getUserByPrincipal(principal);
        Post post = new Post();

        post.setCreator(user);
        post.setTitle(postDTO.getTitle());
        post.setCaption(postDTO.getCaption());
        post.setLocation(postDTO.getLocation());
        post.setLikes(0);

        LOG.info("Saving Post for User : {}", user.getEmail());

        return postRepository.save(post);
    }

    public List<Post> getAllPosts() {
        return postRepository.findAllByOrderByCreatedDesc();
    }

    public Post getPostById(Long postId, Principal principal) {
        User user = userService.getUserByPrincipal(principal);
        return postRepository.findPostByIdAndCreator(postId, user)
                .orElseThrow(() -> new PostNotFoundException("Post cannot be found for user: " + user.getEmail()));
    }

    public List<Post> getUserPosts(Principal principal) {
        User user = userService.getUserByPrincipal(principal);
        return postRepository.findAllByCreatorOrderByCreatedDesc(user);
    }

    public Post likePost(Long postId, String username) {
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new PostNotFoundException("Post cannot be found"));
        Optional<String> userLiked = post.getLikedUsers().stream()
                .filter(u -> u.equals(username)).findAny();
        if (userLiked.isPresent()) {
            post.setLikes(post.getLikes() - 1);
            post.getLikedUsers().remove(username);
        } else {
            post.setLikes(post.getLikes() + 1);
            post.getLikedUsers().add(username);
        }
        return postRepository.save(post);
    }

    public void deletePost(Long postId, Principal principal) {
        Post post = getPostById(postId, principal);
        Optional<ImageModel> imageModel = imageRepository.findByPostId(post.getId());
        postRepository.delete(post);
        imageModel.ifPresent(imageRepository::delete);
    }
}
