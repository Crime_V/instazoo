package cz.com.example.instazoo.service;

import cz.com.example.instazoo.domain.Comment;
import cz.com.example.instazoo.domain.Post;
import cz.com.example.instazoo.domain.User;
import cz.com.example.instazoo.dto.CommentDTO;
import cz.com.example.instazoo.exception.PostNotFoundException;
import cz.com.example.instazoo.repository.CommentRepository;
import cz.com.example.instazoo.repository.PostRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

/**
 * @author : vanya.melnykovych
 * @since : 04.05.2021
 */
@Service
public class CommentService {

    private final UserService userService;

    private final PostRepository postRepository;

    private final CommentRepository commentRepository;

    public static final Logger LOG = LoggerFactory.getLogger(CommentService.class);

    public CommentService(UserService userService, PostRepository postRepository, CommentRepository commentRepository) {
        this.userService = userService;
        this.postRepository = postRepository;
        this.commentRepository = commentRepository;
    }

    public Comment saveComment(Long postId, CommentDTO commentDTO, Principal principal) {
        User user = userService.getUserByPrincipal(principal);
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new PostNotFoundException("post cannot be found by postId : " + postId));

        Comment comment = new Comment();
        comment.setPost(post);
        comment.setUserId(user.getId());
        comment.setUsername(user.getUsername());
        comment.setMessage(commentDTO.getMessage());

        LOG.info("Saving comment for Post : {} ", post.getId());
        return commentRepository.save(comment);
    }

    public List<Comment> getAllPostComments(Long postId) {
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new PostNotFoundException("Post cannot be found by postId : " + postId));
        return commentRepository.findAllByPost(post);
    }

    public void deleteComment(Long commentId) {
        Optional<Comment> comment = commentRepository.findById(commentId);
        comment.ifPresent(commentRepository::delete);
    }
}
